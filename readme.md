Demonstration of POJO to CSV serialization using Jackson
========================================================

Jackson is well know JSON handling library for Java. Lesser is know that it can also serialize and deserialize 
(read/write) CSV (Column Separated Value) files.

This project is a simple demonstration of:  

* line by line CSV file writing, useful for potentially large data set one cannot fit into memory  
* CSV Schema based on annotations, using standard `@JsonProperty`  
* precise and predictable column positioning in the output  
* Jackson classes: 

    * com.fasterxml.jackson.databind.SequenceWriter  
    * com.fasterxml.jackson.dataformat.csv.CsvMapper  
    * com.fasterxml.jackson.dataformat.csv.CsvSchema  