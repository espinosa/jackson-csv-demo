package my.home.evaluation;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CountryStats {
    @JsonProperty(value = "Country name", index = 0)
    String name;

    @JsonProperty(value = "Area in km2", index = 1)
    int area;

    @JsonProperty(value = "Population", index = 2)
    int population;

    public CountryStats(String name, int area, int population) {
        this.name = name;
        this.area = area;
        this.population = population;
    }
}
