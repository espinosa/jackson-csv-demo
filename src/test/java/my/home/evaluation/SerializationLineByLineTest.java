package my.home.evaluation;

import com.fasterxml.jackson.databind.SequenceWriter;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import org.junit.Test;

import java.io.StringWriter;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class SerializationLineByLineTest {

    @Test
    public void test1() throws Exception {
        List<CountryStats> data = Arrays.asList(
                new CountryStats("United Kingdom", 242_495, 67_886_000),
                new CountryStats("Czech Republic", 78_866, 10_693_939),
                new CountryStats("France", 640_679, 67_408_000)
        );
        CsvMapper mapper = new CsvMapper();
        CsvSchema schema = mapper.schemaFor(CountryStats.class).withHeader();
        StringWriter output = new StringWriter();
        SequenceWriter writer = mapper.writer(schema).writeValues(output);
        for (CountryStats row : data) {
            writer.write(row);
        }
        System.out.println(output.toString());
        assertEquals("" +
                        "\"Country name\",\"Area in km2\",Population\n" +
                        "\"United Kingdom\",242495,67886000\n" +
                        "\"Czech Republic\",78866,10693939\n" +
                        "France,640679,67408000\n",
                output.toString());
    }
}
